<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Smev</title>
</head>
<body>
   <?php  header('Content-Type: text/html; charset=UTF-8'); ?>
    <h1>Привет Вадик!</h1>
    <?php 
        
        


       
class CSV {
 
    private $file = null;
 
    /**
     * @param string $csv_file  - путь до csv-файла
     */
    public function __construct($csv_file) {
        if (file_exists($csv_file)) { //Если файл существует
            $this->file = $csv_file; //Записываем путь к файлу в переменную
        } else { //Если файл не найден то вызываем исключение
            throw new Exception("Файл не найден"); 
        }
    }
 
    public function setCSV(Array $csv) {
        //Открываем csv для до-записи, 
        //если указать w, то  ифнормация которая была в csv будет затерта
        $handle = fopen($this->file, "a"); 
 
        foreach ($csv as $value) { //Проходим массив
            //Записываем, 3-ий параметр - разделитель поля
            fputcsv($handle, explode(";", $value), ";"); 
        }
        fclose($handle); //Закрываем
    }
 
    /**
     * Метод для чтения из csv-файла. Возвращает массив с данными из csv
     * @return array;
     */
    public function getCSV() {
        $handle = fopen($this->file, "r"); //Открываем csv для чтения
 
        $array_line_full = array(); //Массив будет хранить данные из csv
        //Проходим весь csv-файл, и читаем построчно. 3-ий параметр разделитель поля
        while (($line = fgetcsv($handle, 0, ";")) !== FALSE) { 
            $array_line_full[] = $line; //Записываем строчки в массив
        }
        fclose($handle); //Закрываем файл
        return $array_line_full; //Возвращаем прочтенные данные
    }
 
}
 
try {
    // echo __DIR__;
    $csv = new CSV("../resources/views/log_2018-07-06.csv"); //Открываем наш csv
    /**
     * Чтение из CSV  (и вывод на экран в красивом виде)
     */
    // echo "<h2>CSV до записи:</h2>";
    // $get_csv = $csv->getCSV();

    // print_r($get_csv);


    $csvFile = fopen('../resources/views/log_2018-07-06.csv', "r");
fgetcsv($csvFile);
while(($row = fgetcsv($csvFile, 1000, ";")) !== FALSE){        
  for ($c=0; $c < count($row); $c++) {
    echo iconv( "Windows-1251", "UTF-8", $row[$c]);
  }
}
fclose($csvFile);
 
    /**
     * Запись новой информации в CSV
     */
    // $arr = array("Антонов Б.А.;Админ OX2.ru;89031233333",
    //     "Колобков В.Б.;Босс OX2.ru;89162233333");
    // $csv->setCSV($arr);
}
catch (Exception $e) { //Если csv файл не существует, выводим сообщение
    echo "Ошибка: " . $e->getMessage();
}

     ?>    
</body>
</html>